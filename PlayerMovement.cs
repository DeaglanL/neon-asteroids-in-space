using UnityEngine;
using System.Collections;
using System.Collections.Generic;
[System.Serializable]
public class Boundary //Min and max distance the player can move on screen
{
	public float xMin, xMax, zMin, zMax; //needs to be set in inspector
}
[System.Serializable]
public class BulletSpawnPosition
{
	[Tooltip("Set the X position of each spawn point")]
	public List <float> xPositions = new List <float> (); //Each element will hold the x and z position of each bullet spawn.
	[Tooltip("Set the Z position of each spawn point")]  //Element 0 = positions of first spawn point in the bulletSpawn array
	public List <float> zPositions = new List <float> ();
}


public class PlayerMovement : MonoBehaviour {

	public float fireRate; //the time between shots made by player
	public Boundary boundary;
	public BulletSpawnPosition BulletSpawnPosition;
	public float speed; //the speed the player moves
	public float tilt; //the amount the ship tilts when moving
	public GameObject bullet; //holds the bullet prefab
	public Transform[] bulletSpawn; //where the bullet spawns from

	private Rigidbody rigidbody;
	private float nextFire = 0.0f; //tracks the time between shots
	// Use this for initialization


	void Awake ()
	{
		rigidbody = GetComponent<Rigidbody> ();
	}
	void Start () {

	}

	void FixedUpdate ()
	{
		Movement();
		Shooting (bullet,bulletSpawn);
	}
	#region Movement
	void Movement()
	{
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");
		
		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
		rigidbody.velocity = movement * speed;
		
		rigidbody.position = new Vector3 
			(
				Mathf.Clamp (rigidbody.position.x, boundary.xMin, boundary.xMax),  //stops player moving off screen
				0.0f, 
				Mathf.Clamp (rigidbody.position.z, boundary.zMin, boundary.zMax)
				);
		
		rigidbody.rotation = Quaternion.Euler (rigidbody.velocity.z * tilt * Time.deltaTime, 0.0f, rigidbody.velocity.x * -tilt * Time.deltaTime); //ships turning on x and z
	}
	#endregion
	#region Shooting
	void Shooting (GameObject bullet,Transform[] spawnPoint) 
	{

		for(int i=0; i < spawnPoint.Length;++i)
		{
			//Sets each spawn point transform to the values set in the inspector for the x and z axis
			spawnPoint[i].position = new Vector3 (gameObject.transform.position.x + BulletSpawnPosition.xPositions[i] ,gameObject.transform.position.y,gameObject.transform.position.z + BulletSpawnPosition.zPositions[i]);
		}
		if(Input.GetButtonDown("Jump")&& Time.time > nextFire) //if player hits space and time is passed fire rate
		{
			nextFire = Time.time + fireRate;
			foreach(Transform spawn in spawnPoint) //spawn a bullet at the location of each bullet spawn
			{
				Instantiate(bullet,spawn.position,spawn.rotation);
			}

		}
	}
	#endregion

	
	// Update is called once per frame
	void Update () {
	
	}
}
